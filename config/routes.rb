Rails.application.routes.draw do
  resources :invoices, only: %w[index create] 
  resources :payments, only: :create
end
