class InvoicesController < ApplicationController
  def index
    @invoices = Invoice.includes(:payments).filterable(params.slice(:payed))
    render json: @invoices
  end

  def create
    invoices = []
    params[:invoices].each do |invoice_params|
      invoice = Invoice.find_or_initialize_by(invoice_number: invoice_params[:invoice_number])
      invoice.update(permitted_params(invoice_params))
      invoices << { invoice: invoice, errors: invoice.errors }
    end
    render json: invoices
  end

  private
 
  def permitted_params(invoice_params)
    invoice_params.permit(:invoice_number, :invoice_date, :total_amount, :open_amount)
  end
end