class PaymentsController < ApplicationController
  def create
    @invoice = Invoice.find_by(invoice_number: params[:invoice_number])
    if @invoice
      @payment = @invoice.payments.create(payment_params)
      @invoice.update_open_amount if @payment.save
      render json: @payment
    else
      render json: { error: 'Invoice not found' }
    end
  end

  private

  def payment_params
    params.require(:payment).permit(:amount, :payment_date, :payment_method, :currency_code)
  end
end