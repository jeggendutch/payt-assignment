module InvoiceConcern
  extend ActiveSupport::Concern
  
  def update_open_amount
    self.update(open_amount: self.total_amount - payments.sum(:amount))
  end
end