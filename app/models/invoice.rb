class Invoice < ApplicationRecord
  has_many :payments, dependent: :destroy
  validates_presence_of :invoice_number
  include Filterable
  include InvoiceConcern

  scope :payed, lambda {|payed|
    payed.to_s.downcase == "true" ? where(open_amount: 0) : where.not(open_amount: 0)
  }
end
