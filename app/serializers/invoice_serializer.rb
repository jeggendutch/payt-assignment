class InvoiceSerializer < ActiveModel::Serializer
  attributes :id, :open_amount, :total_amount, :currency_code, :invoice_number, :invoice_date, :payments
end