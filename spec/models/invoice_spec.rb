require 'rails_helper'

RSpec.describe Invoice, type: :model do
  it "is invalid without an invoice_number" do
    invoice = build(:invoice, invoice_number: nil)
    expect(invoice.valid?).to eq false
  end

  context 'payed scope' do
    it "returns invoices that are payed" do
      invoice = create(:invoice)
      invoice2 = create(:invoice, open_amount: 0)

      expect(Invoice.payed(true)).to include(invoice2)
      expect(Invoice.payed(true)).to_not include(invoice)
    end

    it "returns invoices that are not fully payed" do
      invoice = create(:invoice)
      invoice2 = create(:invoice, open_amount: 0)

      expect(Invoice.payed(false)).to_not include(invoice2)
      expect(Invoice.payed(false)).to include(invoice)
    end
  end

  it "extracts payment amount from invoice open_amount" do
    invoice = create(:invoice, total_amount: 200)
    create_list(:payment, 2, invoice: invoice, amount: 50)

    invoice.update_open_amount
    expect(invoice.open_amount).to eq 100
  end
end
