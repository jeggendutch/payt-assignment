FactoryBot.define do
  factory :payment do
    amount { 50 }
    payment_date { Time.now }
    payment_method { 'ideal' }
    currency_code { 'eur' }
  end
end
