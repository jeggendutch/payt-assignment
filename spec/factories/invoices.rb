FactoryBot.define do
  factory :invoice do
    total_amount { 100 }
    open_amount { 50 }
    invoice_date { Time.now }
    invoice_number { rand(36**10).to_s(36) }
  end
end
