require 'rails_helper'

RSpec.describe InvoicesController, type: :controller do
  describe 'GET #index' do
    let!(:invoice) { create(:invoice) }

    it 'assigns invoices' do
      get :index, xhr: true, :format => :json
      expect(assigns(:invoices)).to eq [invoice]
    end

    it 'filters payed invoices' do
      invoice.update(open_amount: 0)
      get :index, xhr: true, params: { payed: true }, :format => :json
      expect(assigns(:invoices)).to eq [invoice]
    end
  end

  describe 'POST #create' do
    it 'creates the invoice' do
      post :create, xhr: true, params: { invoices: [{ invoice_number: 'xyz234', total_amount: 200 }] }, :format => :json
      expect(Invoice.count).to eq 1
    end

    it 'returns error message if record is invalid' do
      post :create, xhr: true, params: { invoices: [{ total_amount: 200 }] }, :format => :json
      expect(JSON(response.body)[0]['errors']).to_not eq nil
      expect(JSON(response.body)[0]['errors']['invoice_number']).to eq(["can't be blank"])
    end

    it 'returns the invoice' do
      post :create, xhr: true, params: { invoices: [{ invoice_number: 'invoice123456', total_amount: 200 }] }, :format => :json
      expect(JSON(response.body)[0]).to_not eq nil
      expect(JSON(response.body)[0]['invoice']['invoice_number']).to eq 'invoice123456'
    end
  end
end
