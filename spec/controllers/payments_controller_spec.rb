require 'rails_helper'

RSpec.describe PaymentsController, type: :controller do
  describe 'POST #create' do
    let!(:invoice) { create(:invoice, total_amount: 200) }

    it 'creates the payment' do
      post :create, xhr: true, params: { invoice_number: invoice.invoice_number, payment: { amount: 100 } }, :format => :json
      expect(Payment.count).to eq 1
    end

    it 'returns error message if invoice cant be found' do
      post :create, xhr: true, params: { invoice_number: 'wrong invoice', payment: { amount: 100 } }, :format => :json
      expect(JSON(response.body)['error']).to eq 'Invoice not found'
    end

    it 'returns the invoice' do
      post :create, xhr: true, params: { invoice_number: invoice.invoice_number, payment: { amount: 100, currency_code: 'usd', payment_method: 'paypal' } }, :format => :json
      expect(JSON(response.body)['amount'].to_i).to eq 100
      expect(JSON(response.body)['currency_code']).to eq 'usd'
      expect(JSON(response.body)['payment_method']).to eq 'paypal'
    end

    it 'updates the invoice open_amount' do
      post :create, xhr: true, params: { invoice_number: invoice.invoice_number, payment: { amount: 200 } }, :format => :json
      invoice.reload
      expect(invoice.open_amount).to eq 0
    end
  end
end