class CreateInvoices < ActiveRecord::Migration[6.0]
  def change
    create_table :invoices do |t|
      t.decimal :open_amount, precision: 10, scale: 2, default: 0
      t.decimal :total_amount, precision: 10, scale: 2, default: 0
      t.string :currency_code, default: 'eur'
      t.string :invoice_number
      t.datetime :invoice_date
      
      t.timestamps
    end
  end
end
