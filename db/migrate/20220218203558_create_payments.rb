class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.string :currency_code, default: 'eur'
      t.datetime :payment_date
      t.string :payment_method
      t.references :invoice, index: true, add_foreign_key: true

      t.timestamps
    end
  end
end
